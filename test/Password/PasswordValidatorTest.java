/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Password;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author eshka
 */
public class PasswordValidatorTest {
    
    public PasswordValidatorTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testValidatePasswordLengthRegular() {
        System.out.println("validatePassword length regular");
        String password = "abcdefghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthException() {
        System.out.println("validatePassword length exception");
        String password = "";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthBoundaryIn() {
        System.out.println("validatePassword length boundary in");
        String password = "abcdefgh";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthBoundaryOut() {
        System.out.println("validatePassword length boundary out");
        String password = "abcdefg";
        boolean expResult = false;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
    
    @Test
    public void testValidatePasswordLengthSpecialCharsRegular() {
        System.out.println("validatePassword length specail chars regular");
        String password = "abcd@efghij";
        boolean expResult = true;
        boolean result = PasswordValidator.validatePassword(password);
        assertEquals(expResult, result);
        fail("The test case is a prototype.");
    }
}
